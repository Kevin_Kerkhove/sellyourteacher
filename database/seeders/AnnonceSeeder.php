<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Annonce;

class AnnonceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Annonce::create(
            [
                "id" => "1",
                "professeur_id" => "1",
                "nom_cours" => "Les maths",
                "titre" => "trigonométrie",
                "description" => "cours de 2h sur le chapitre principal de trigonométrie",
                "prix" => "25",
                "matiere" => "mathématique",
                "etudiant_id" => "1"
            ]
        );
        Annonce::create(
            [
                "id" => "2",
                "professeur_id" => "1",
                "nom_cours" => "Les maths",
                "titre" => "Les suite",
                "description" => "cours de 2h sur le chapitre principal des suite",
                "prix" => "25",
                "matiere" => "mathématique",
                "etudiant_id" => null,
            ]
        );
        Annonce::create(
            [
                "id" => "3",
                "professeur_id" => "1",
                "nom_cours" => "Anglais",
                "titre" => "Verbes irrégulier",
                "description" => "cours de 2h sur les verbes irréguliers en anglais",
                "prix" => "25",
                "matiere" => "anglais",
                "etudiant_id" => null,
            ]
        );
        Annonce::create(
            [
                "id" => "4",
                "professeur_id" => "1",
                "nom_cours" => "Cours de prog",
                "titre" => "Prog Java",
                "description" => "cours de 2h de prog Java niveau DUT",
                "prix" => "25",
                "matiere" => "Prog orientée objet",
                "etudiant_id" => null,
            ]
        );
    }
}
