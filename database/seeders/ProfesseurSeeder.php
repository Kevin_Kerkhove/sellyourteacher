<?php

namespace Database\Seeders;

use App\Models\Professeur;
use Illuminate\Database\Seeder;

class ProfesseurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Professeur::create(
            [
                "id"=>"1",
                "nom" => "Lopez",
                "prenom" => "Joe",
                "user_id"=>"2",
            ]
        );
    }
}
