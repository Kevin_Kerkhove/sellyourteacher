<?php

namespace Database\Seeders;

use App\Models\Etudiant;
use Illuminate\Database\Seeder;

class EtudiantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Etudiant::create(
            [
                "id"=>"1",
                "nom" => "Kerkhove",
                "prenom" => "Kevin",
                "dateDeNaissance"=>"2001-01-04",
                "niveau"=>"LPDIOC",
                "user_id"=>"3",
            ]
        );
    }
}
