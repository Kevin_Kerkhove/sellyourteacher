<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnoncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->id();
            
            $table->string('titre');
            $table->string('matiere');
            $table->string('description');
            //$table->string('fichier');
            $table->string('nom_cours');          
            $table->float('prix');

            $table->foreign('professeur_id')->references('id')->on('professeurs')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('professeur_id')->unsigned()->nullable(false);
            
            $table->foreign('etudiant_id')->references('id')->on('etudiants')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('etudiant_id')->unsigned()->nullable(true);

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
