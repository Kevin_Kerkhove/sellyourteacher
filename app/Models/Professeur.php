<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Professeur extends Model
{
    use HasFactory;
    protected $table = 'professeurs';

    function user(){
        return $this->belongsTo(User::class);
    }

    function annonces(){
        return $this->hasMany(Annonce::class);
    }
}
