<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    use HasFactory;

    protected $table = 'annonces';

    function professeur(){
        return $this->belongsTo(Professeur::class);
    }

    function etudiant(){
        return $this->belongsTo(Etudiant::class);
    }
}
