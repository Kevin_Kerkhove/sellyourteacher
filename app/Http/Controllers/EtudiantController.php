<?php

namespace App\Http\Controllers;

use App\Models\Etudiant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EtudiantController extends Controller
{
    public function index(Request $request) {
        $etudiant = Etudiant::all();
        return view('etudiant.index', ['etudiant'=>$etudiant]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('etudiant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['nom'=>'required',
                'prenom'=>'required',
                'dateDeNaissance'=>'required',
                'niveau'=>'required',
                'email'=>'required',
                'motDePasse'=>'required',
            ]
        );

        $etudiant = new Etudiant();

        $etudiant->nom = $request->nom;
        $etudiant->prenom = $request->prenom;
        $etudiant->dateDeNaissance = $request->dateDeNaissance;
        $etudiant->niveau = $request->niveau;

        $etudiant->save();

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request ,$id)
    {
        $action = $request->query('action','show');
        $etudiant = Etudiant::find($id);
        $user= User::find($etudiant->user_id);
        $annonces = $etudiant->annonces;
        return view('etudiant.show',['action'=>$action,'etudiant'=>$etudiant,'user'=>$user,'annonces'=>$annonces]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $etudiant = Etudiant::find($id);
        return view('etudiant.edit',['etudiant'=>$etudiant]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            ['nom'=>'required',
                'prenom'=>'required',
                'dateDeNaissance'=>'required',
                'niveau'=>'required',
                'email'=>'required',
                'motDePasse'=>'required',
                'remember_mdp'=>'required|min:4'
            ]
        );

        $etudiant = new Etudiant();

        $etudiant->nom = $request->nom;
        $etudiant->prenom = $request->prenom;
        $etudiant->dateDeNaissance = $request->dateDeNaissance;
        $etudiant->niveau = $request->niveau;
        $etudiant->email = $request->email;
        $etudiant->motDePasse = $request->motDePasse;
        $etudiant->remember_mdp = $request->remember_mdp;

        $etudiant->save();

        return redirect('/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->delete == 'valide'){
            $etudiant = Etudiant::find($id);
            $etudiant->delete();
        }
        return redirect('/');
    }




}
