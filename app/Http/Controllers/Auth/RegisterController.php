<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Etudiant;
use App\Models\Professeur;
use App\Models\Role;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'prenom'=>['required','string','max:255'],
            'dateDeNaissance'=>['required','date'],
            'niveau'=>['required','string','max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role'=>['required','string'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        if($data['role'] == 'etudiant')
        {
            $etudiant = new Etudiant();

            $etudiant->nom = $data['name'];
            $etudiant->prenom = $data['prenom'];
            $etudiant->dateDeNaissance = $data['dateDeNaissance'];
            $etudiant->niveau = $data['niveau'];
            $etudiant->user_id = $user->id;

            $etudiant->save();

            $roles = Role::select('id')->where('name', 'etudiant')->first();
            $user->roles()->attach($roles);
            return $user;
        }
        elseif ($data['role']=='professeur')
        {
            $professeur = new Professeur();

            $professeur->nom = $data['name'];
            $professeur->prenom = $data['prenom'];
            $professeur->user_id = $user->id;

            $professeur->save();

            $roles = Role::select('id')->where('name', 'professeur')->first();
            $user->roles()->attach($roles);
            return $user;
        }
    }
}
