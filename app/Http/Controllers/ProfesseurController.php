<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Models\Professeur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfesseurController extends Controller
{
    public function index(Request $request)
    {
        $professeur = Professeur::all();
        return view('professeur.index', ['professeur' => $professeur]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('professeur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['nom'=>'required',
                'prenom'=>'required',
                'mail'=>'required',
                'motDePasse'=>'required',
                'remember_mdp'=>'required|min:4'
            ]
        );

        $professeur = new Professeur();

        $professeur->nom = $request->nom;
        $professeur->prenom = $request->prenom;
        $professeur->mail = $request->mail;
        $professeur->motDePasse = $request->motDePasse;
        $professeur->remember_mdp = $request->remember_mdp;

        $professeur->save();

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request ,$id)
    {
        $action = $request->query('action','show');
        $professeur = Professeur::find($id);
        $annonces = $professeur->annonces;
        $user=Auth::user();

        return view('professeur.show',['action'=>$action,'professeur'=>$professeur,'user'=>$user,'annonces'=>$annonces]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professeur = Professeur::find($id);
        return view('professeur.edit',['professeur'=>$professeur]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            ['nom'=>'required',
                'prenom'=>'required',
                'mail'=>'required',
                'motDePasse'=>'required',
                'remember_mdp'=>'required|min:4'
            ]
        );

        $professeur = new Professeur();

        $professeur->nom = $request->nom;
        $professeur->prenom = $request->prenom;
        $professeur->mail = $request->mail;
        $professeur->motDePasse = $request->motDePasse;
        $professeur->remember_mdp = $request->remember_mdp;

        $professeur->save();

        return redirect('/');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->delete == 'valide'){
            $professeur = Professeur::find($id);
            $professeur->delete();
        }
        return redirect('/');
    }
}
