<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Monolog\Handler\RollbarHandler;

class UsersController extends Controller
{
    public function __construct(){
        $this->middleware(('auth'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $u = Auth::user();
        $user = User::find($u->id);
        if($user->isEtudiant()){
            $etudiant = $user->etudiant;
            $annonces = $etudiant->annonces;
            return view('user.show',['etudiant'=>$etudiant,'user'=>$user,'annonces'=>$annonces]);
        }
        elseif($user->isProfesseur()){
            $professeur = $user->professeur;
            $annonces = $professeur->annonces;
            return view('user.show',['professeur'=>$professeur,'user'=>$user,'annonces'=>$annonces]);
        }




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(Gate::denies('admin-users')){
            return redirect()->route('admin.users.index');
        }

        $roles = Role::all();

        return view('admin.users.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->roles);

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('admin-users')){
            return redirect()->route('admin.users.index');
        }
        $user = User::find($id);
        if($user->isProfesseur()){
            $prof = $user->professeur;
            $prof->delete();
            $user->roles()->detach();
            $user->delete();

            return redirect('/admin/users');
        }
        elseif($user->isEtudiant()){
            $etu = $user->etudiant;
            $etu->delete();
            $user->roles()->detach();
            $user->delete();

            return redirect('/admin/users');
        }
    }
}
