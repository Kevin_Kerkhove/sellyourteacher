<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Annonce;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class AnnonceController extends Controller
{
    public function index()
    {
        $annonces = DB::table('annonces')->orderBy('created_at', 'DESC')->simplePaginate(2);

        return view('annonces.annonce', compact('annonces'));
    }

    public function create()
    {
        return view('annonces.create');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            ['titre'=>'required',
                'matiere'=>'required',
                'description'=>'required',
                'prix'=>'required',
                'nom_cours'=>'required',
            ]
        );

        $u = Auth::user();
        $user = User::find($u->id);
        $professeur = $user->professeur;
        $annonce = new Annonce();

        $annonce->titre = $request->titre;
        $annonce->description = $request->description;
        $annonce->prix = $request->prix;
        $annonce->matiere = $request->matiere;
        $annonce->nom_cours = $request->nom_cours;
        $annonce->professeur_id = $professeur->id;
        $annonce->save();

        return redirect('/annonces')->with('success', 'Votre annonce a bien été enregistrer');
    }

    public function show(Request $request, $id)
    {
        $action = $request->query('action','show');
        $annonce = Annonce::find($id);
        $etudiant = $annonce->etudiant;
        $professeur = $annonce->professeur;
        $u=Auth::user();
        $user = User::find($u->id);
        $userProf = $user->professeur;

        return view('annonces.show', ['action'=>$action,'annonce'=>$annonce,'user'=>$user,'professeur'=>$professeur,'etudiant'=>$etudiant,'userProf'=>$userProf]);
    }

    public function inscription($id)
    {
        $annonce = Annonce::find($id);
        $u = Auth::user();
        $user = User::find($u->id);
        $etu = $user->etudiant;
        $annonce->etudiant()->associate($etu);
        $etu->annonces()->save($annonce);

        return redirect('/annonces')->with('success', 'Votre inscription a bien été enregistrer');
    }

    public function destroy($id){
        $annonce = Annonce::find($id);
        $annonce->delete();
        return redirect('/annonces');


    }
}
