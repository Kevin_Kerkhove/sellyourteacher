@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1>Liste des annonces</h1>
      <hr>
      @foreach ($annonces as $annonce)
      <div class="card" style="width: 90%;">
          <div class="card-body">
            <h5 class="text-success"> {{ $annonce->matiere }}</h5>
            <h5 class="card-title">{{ $annonce->titre }}</h5>
            <small>{{ Carbon\Carbon::parse($annonce->created_at)->diffForHumans() }}</small>
            <p class="card-text">{{ $annonce->description }}</p>
            <p class="card-text">{{ $annonce->prix }} €</p>
            <a href="{{ route('annonces.show', $annonce->id) }}" class="stretched-link btn btn-info">{{__('Voir l\'annonce') }}</a>
          </div>
          <hr>
      @endforeach
    </div>
  </div>
</div>
<div class="row justify-content-center">
{{ $annonces->appends(request()->input())->links() }}
</div>
@endsection    
