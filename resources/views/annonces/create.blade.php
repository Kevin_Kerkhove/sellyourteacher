@extends('layouts.app')

@section('content')
    <h1 class="py-3"> Création d'une annonce </h1>
    <hr>

    <form class="container" method="POST" action="{{ route('annonce.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="titre">{{__('Titre de l\'annonce')}}</label>
            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" aria-describedby="titre">
            @error('titre')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">{{__('Description de l\'annonce')}}</label>
            <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" cols="30" rows="10"></textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="prix">{{__('Prix de l\'annonce')}}</label>
            <input type="number" min="1" step="0.01" class="form-control @error('prix') is-invalid @enderror" id="prix" name="prix">
            @error('prix')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="nom_cours">Nom du cours</label>
            <input type="text" class="form-control @error('nom_cours') is-invalid @enderror" id="nom_cours" name="nom_cours">
            @error('nom_cours')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="matiere">Matière</label>
            <input type="text" class="form-control @error('matiere') is-invalid @enderror" id="matiere" name="matiere">
            @error('matiere')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <!--<div class="form-group">
            <label for="fichier">lien du cours</label>
            <input type="file" name="fichier" class="form-control @error('fichier') is-invalid @enderror">
            @error('fichier')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>-->

        
        <button type="submit" class="btn btn-primary">{{__('Soumettre l\'annonce')}} </button>
    </form>
@endsection
