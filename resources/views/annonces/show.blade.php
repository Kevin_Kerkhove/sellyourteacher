@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
      <div class="col p-4 d-flex flex-column position-static">
        <div class="jumbotron">
        <strong class="d-inline-block mb-2 text-success">{{ $annonce->matiere }}</strong>
        <h5 class="mb-0">{{ $annonce->titre }}</h5>
        <hr>
        <p class="mb-auto">{{ $annonce->description }}</p>
        <p class="mb-auto py-3">Professeur: {{ $professeur->prenom }} {{ $professeur->nom }}</p>
        <p class="mb-auto py-3"> </p>
        <strong class="mb-auto center font-weight-normal text-secondary">Prix : {{ $annonce->prix }} €</strong>
        @if($professeur == $userProf || $user->isAdmin())
        @if($annonce->etudiant!=null)
        <p class="mb-auto py-3">Etudiant: {{ $etudiant->prenom }} {{ $etudiant->nom  }}/ Contacter par mail: <a href="mailTo:{{$etudiant->user->email}}" >{{$etudiant->user->email}}</a></p>
        @endif
        @else
                @if($annonce->etudiant!=null)
            <p class="mb-auto py-3">Etudiant: {{ $etudiant->prenom }} {{ $etudiant->nom  }}</p>
                @endif
        @endif

        </div>
          @if($user->isEtudiant() && $annonce->etudiant==null)

              <a href="{{ route('annonce.inscription', $annonce->id) }}" >
                  <button type="button" class="btn btn-success">
                      S'inscrire à ce cours
                  </button>
              </a>
          @endif
          @if($professeur == $userProf || $user->isAdmin())
              <a href="{{ route('annonce.destroy', $annonce->id) }}" >
                  <button type="button" class="btn btn-danger">
                      Supprimer l'annonce
                  </button>
              </a>

          @endif
    </div>
  </div>
</div>
@endsection
