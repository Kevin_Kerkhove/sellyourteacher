@extends('layouts.app')

@section('content')
@guest

<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">Bienvenue sur {{ config('app.name')  }}</h1>
        <p style="text-align: center;">Un site web simple de cours particulier en ligne.</p>
    </div>
</div>
@else
<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">Bienvenue sur {{ config('app.name')  }}</h1>
        <p style="text-align: center;">Un site web simple de cours particulier en ligne.</p>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <center>{{ __('Vous êtes connecté(e)') }}</center>
                </div>
            </div>
        </div>
    </div>
</div>


@endguest
@endsection
