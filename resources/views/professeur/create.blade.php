@extends('layouts.app')

@section('content')
    <form class="container" method="POST" action="{{ route('professeur.store') }}" enctype="multipart/form-data">
        @csrf
        <h1>"Ajout D'un Professeur"</h1>
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom" name="nom" aria-describedby="nom">
            @error('nom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="prenom">Prénom</label>
            <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="prenom" name="prenom" aria-describedby="prenom">
            @error('prenom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>


        <div class="form-group">
            <label for="mail">Adresse E-Mail</label>
            <input type="email" class="form-control @error('mail') is-invalid @enderror" id="mail" name="mail">
            @error('mail')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="motDePasse">Mot De Passe</label>
            <input type="password"  class="form-control @error('motDePasse') is-invalid @enderror" id="motDePasse" name="motDePasse">
            @error('motDePasse')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="remember_mdp">Confirmer Le Mot De Passe</label>
            <input type="password"  class="form-control @error('remember_mdp') is-invalid @enderror" id="remember_mdp" name="remember_mdp">
            @error('remember_mdp')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">"S'inscrire"</button>
    </form>
@endsection


