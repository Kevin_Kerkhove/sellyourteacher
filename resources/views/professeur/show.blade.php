@extends('layouts.app')
@section('content')
    <h1><strong>Mon Profile</strong></h1>
    <div class="col-md-12">
        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
                <h5 class="mb-0"><strong>Professeur</strong></h5>
                <hr>
                <p class="mb-auto text-muted"><strong>Nom: </strong>{{$professeur->nom}}</p>
                <hr>
                <p class="mb-auto text-muted"><strong>Prenom: </strong>{{$professeur->prenom}}</p>
                <hr>
                <p class="mb-auto text-muted"><strong>Adresse Mail: </strong>{{$professeur->mail}}</p>
                <hr>
            </div>
        </div>
    </div>
    
    <div>
        <h1><strong>Mes Annonces</strong></h1>
        @foreach($annonces as $annonce)
            <div class="col-md-12">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-success">{{$annonce->matiere}}</strong>
                        <h5 class="mb-0">{{ $annonce->titre }}</h5>
                        <hr>
                        <p class="mb-auto text-muted">{{ $annonce->description }}</p>
                        <strong class="mb-auto font-weight-normal text-secondary">{{ $annonce->prix }}</strong>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
