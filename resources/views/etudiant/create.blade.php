@extends('layouts.app')

@section('content')

    <form class="container" method="POST" action="{{ route('etudiant.store') }}" enctype="multipart/form-data">
        @csrf
        <h1>Inscription etudiant</h1>
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom" name="nom" aria-describedby="nom">
            @error('nom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="prenom">Prénom</label>
            <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="prenom" name="prenom" aria-describedby="prenom">
            @error('prenom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        {{-- date de naissance --}}
        <div class="form-group">
            <label for="dateDeNaissance">Date De Naissance</label>
                <input class="form-control @error('dateDeNaissance') is-invalid @enderror" type="date" value="2011-08-19" id="dateDeNaissance" name="dateDeNaissance">
            @error('dateDeNaissance')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="niveau">Niveau</label>
            <input type="text" class="form-control @error('niveau') is-invalid @enderror" id="niveau" name="niveau">
            @error('niveau')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="email">Adresse E-Mail</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="motDePasse">Mot De Passe</label>
            <input type="password"  class="form-control @error('motDePasse') is-invalid @enderror" id="motDePasse" name="motDePasse">
            @error('motDePasse')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmer Le Mot De Passe') }}</label>
            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>


        <button type="submit" class="btn btn-primary">S'inscrire</button>
    </form>
@endsection



