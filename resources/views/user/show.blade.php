@extends('layouts.app')
@section('content')
    @if($user->isEtudiant())
        <h1><strong>Mon Profile</strong></h1>
        <div class="col-md-12">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="col p-4 d-flex flex-column position-static">
                    <h5 class="mb-0"><strong>Etudiant</strong></h5>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Nom: </strong>{{$user->name}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Prenom: </strong>{{$etudiant->prenom}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Date de Naissance: </strong>{{$etudiant->dateDeNaissance}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Niveau: </strong>{{$etudiant->niveau}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Adresse Mail: </strong>{{$user->email}}</p>
                    <hr>
                </div>
            </div>
        </div>

        <div>
            <h1><strong>Mes Cours</strong></h1>
                @foreach($annonces as $annonce)
                    <div class="col-md-12">
                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col p-4 d-flex flex-column position-static">
                                <strong class="d-inline-block mb-2 text-success">{{$annonce->matiere}}</strong>
                                <h5 class="mb-0">{{ $annonce->titre }}</h5>
                                <hr>
                                <p class="mb-auto text-muted">{{ $annonce->description }}</p>
                                <strong class="mb-auto font-weight-normal text-secondary">{{ $annonce->prix }}</strong>
                                <a href="{{ route('annonces.show', $annonce->id) }}" class="stretched-link btn btn-info">{{__('Voir L\'annonce') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    @elseif($user->isProfesseur())
        <h1><strong>Mon Profile</strong></h1>
        <div class="col-md-12">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="col p-4 d-flex flex-column position-static">
                    <h5 class="mb-0"><strong>Professeur</strong></h5>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Nom: </strong>{{$professeur->nom}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Prénom: </strong>{{$professeur->prenom}}</p>
                    <hr>
                    <p class="mb-auto text-muted"><strong>Adresse E-Mail: </strong>{{$user->email}}</p>
                    <hr>
                </div>
            </div>
        </div>
        <div>
            <h1><strong>Mes Annonces</strong></h1>

                @foreach($annonces as $annonce)
                    <div class="col-md-12">
                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col p-4 d-flex flex-column position-static">
                                <strong class="d-inline-block mb-2 text-success">{{$annonce->matiere}}</strong>
                                <h5 class="mb-0">{{ $annonce->titre }}</h5>
                                <hr>
                                <p class="mb-auto text-muted">{{ $annonce->description }}</p>
                                <strong class="mb-auto font-weight-normal text-secondary">{{ $annonce->prix }}</strong>
                                <a href="{{ route('annonces.show', $annonce->id) }}" class="stretched-link btn btn-info">{{__('Voir L\'annonce') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach

        </div>
    @endif
@endsection
