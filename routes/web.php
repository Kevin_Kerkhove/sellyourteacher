<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/annonce/create', [App\Http\Controllers\AnnonceController::class, 'create'])->name('annonce.create');
Route::post('/annonce/create', [App\Http\Controllers\AnnonceController::class, 'store'])->name('annonce.store');
Route::get('/annonces', [App\Http\Controllers\AnnonceController::class, 'index'])->name('annonce.index');
Route::post('/annonces', [App\Http\Controllers\AnnonceController::class, ''])->name('annonces');
Route::get('/annonce/{id}', [App\Http\Controllers\AnnonceController::class, 'show'])->name('annonces.show');
Route::get('/annonce/{id}/inscription', [App\Http\Controllers\AnnonceController::class, 'inscription'])->name('annonce.inscription');
Route::get('/annonce/supprimer/{id}', 'App\Http\Controllers\AnnonceController@destroy')->name('annonce.destroy');

Route::get('/admin/users/{id}', 'App\Http\Controllers\Admin\UsersController@destroy')->name('users.destroy');

Route::post('/professeur', 'App\Http\Controllers\ProfesseurController@store')->name('professeur.store');

Route::get('/profile','App\Http\Controllers\Admin\UsersController@show')->name('user.show');

Route::get('/professeur/profile/{id}', 'App\Http\Controllers\ProfesseurController@show')->name('professeur.show');

Route::get('/professeur/edit/{id}', 'App\Http\Controllers\ProfesseurController@edit')->name('professeur.edit');

Route::get('/professeur/update/{id}', 'App\Http\Controllers\ProfesseurController@update')->name('professeur.update');

Route::get('/professeur/destroy/{id}', 'App\Http\Controllers\ProfesseurController@destroy')->name('professeur.destroy');

Route::get('/professeur/create', 'App\Http\Controllers\ProfesseurController@create')->name('professeur.create');

Route::post('/etudiant', 'App\Http\Controllers\EtudiantController@store')->name('etudiant.store');

Route::get('/etudiant/profile/{id}', 'App\Http\Controllers\EtudiantController@show')->name('etudiant.show');

Route::get('/etudiant/edit/{id}', 'App\Http\Controllers\EtudiantController@edit')->name('etudiant.edit');

Route::get('/etudiant/update/{id}', 'App\Http\Controllers\EtudiantController@update')->name('etudiant.update');

Route::get('/etudiant/destroy/{id}', 'App\Http\Controllers\EtudiantController@destroy')->name('etudiant.destroy');

Route::get('/etudiant/create', 'App\Http\Controllers\EtudiantController@create')->name('etudiant.create');

Route::resource('/admin/users', 'App\Http\Controllers\Admin\UsersController');

Route::namespace('App\Http\Controllers\Admin')->prefix('/admin')->name('admin.')->middleware('can:admin-users')->group(function() {
    Route::resource('users', 'UsersController');
});

